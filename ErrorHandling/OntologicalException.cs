﻿using Localization_Module;
using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessTree_Module.ErrorHandling
{
    public class OntologicalException : Exception
    {
        
        public OntologicalException(MessageItem messageItem) : base(messageItem != null ? messageItem.Message : "")
        {
        }
    }
}
