﻿using OntologyAppDBConnector;
using OntoMsg_Module;
using ProcessTree_Module.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ProcessTree_Module
{
    public class UserControl_ProcessTreeViewModel
    {
        private clsLocalConfig localConfig;
        private ObjectSubscriber processTreeNodeSubscirber;
        private ProcessTreeFactory processTreeFactory;

        public UserControl_ProcessTreeViewModel()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }
            Initialize();
        }

        private void Initialize()
        {
            processTreeNodeSubscirber = MessageManager.RegisterSubscriber(localConfig.ProcessTreeNodePublisher);
            processTreeNodeSubscirber._exchangeOItem += Subscriber__exchangeOItem;
            processTreeFactory = new ProcessTreeFactory(localConfig);

        }

        private void Subscriber__exchangeOItem(OntologyClasses.BaseClasses.clsOntologyItem oItem)
        {
            if (oItem.GUID == localConfig.Globals.Root.GUID)
            {
                var result = processTreeFactory.CreatePublicProcessTree();

            }   
            else
            {

            }
        }
    }
}
