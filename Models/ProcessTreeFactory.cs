﻿using ImageMapper_Module.BaseClasses;
using OntologyClasses.BaseClasses;
using ProcessTree_Module.Collectors;
using ProcessTree_Module.Communications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessTree_Module.Models
{
    public class ProcessTreeFactory : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private DataCollector_ProcessTree dataCollectorProcessTree;

        private List<ProcessTreeNode> publicProcesses;
        public List<ProcessTreeNode> PublicProcesses
        {
            get { return publicProcesses; }
            private set
            {
                publicProcesses = value;
                RaisePropertyChanged(NotifyChanges.ProcessTreeFactory_PublicProcesses);
            }
        }

        public clsOntologyItem CreatePublicProcessTree()
        {
            var result = dataCollectorProcessTree.GetData_PublicProcesses();

            return result;
        }

        public ProcessTreeFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dataCollectorProcessTree = new DataCollector_ProcessTree(localConfig);
            dataCollectorProcessTree.PropertyChanged += DataCollectorProcessTree_PropertyChanged;
        }

        private void DataCollectorProcessTree_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.DataCollector_ProcessTree_Result_LoadPublicProcesses)
            {
                if (dataCollectorProcessTree.Result_LoadPublicProcesses.GUID == localConfig.Globals.LState_Success.GUID)
                {
                    PublicProcesses = dataCollectorProcessTree.PublicProcesses.Select(pubProc => new ProcessTreeNode(pubProc, localConfig, dataCollectorProcessTree)).ToList();
                }
                else if (dataCollectorProcessTree.Result_LoadPublicProcesses.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    PublicProcesses = new List<ProcessTreeNode>();
                }
            }
        }
    }
}
