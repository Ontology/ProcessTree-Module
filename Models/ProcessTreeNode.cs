﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using ProcessTree_Module.BaseClasses;
using ProcessTree_Module.Collectors;
using ProcessTree_Module.Communications;
using ProcessTree_Module.ErrorHandling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessTree_Module.Models
{
    
    public class ProcessTreeNode : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private DataCollector_ProcessTree dataCollectorProcessTree;

        public string IdProcessTreeNode
        {
            get { return proxyItem.GUID; }
            set
            {
                proxyItem.GUID = value;
                RaisePropertyChanged(NotifyChanges.ProcessTreeNode_IdProcessTreeNode);
            }
        }

        private string nameProcessTreeNode;
        public string NameProcessTreeNode
        {
            get { return nameProcessTreeNode; }
            set
            {
                nameProcessTreeNode = value;
                RaisePropertyChanged(NotifyChanges.ProcessTreeNode_NameProcessTreeNode);
            }
        }

        private string idParentProcessTreeNode;
        public string IdParentProcessTreeNode
        {
            get { return idParentProcessTreeNode; }
            set
            {
                idParentProcessTreeNode = value;
                RaisePropertyChanged(NotifyChanges.ProcessTreeNode_IdParentProcessTreeNode);
            }
        }

        private clsOntologyItem proxyItem;
        public clsOntologyItem ProxyItem
        {
            get { return proxyItem; }
            private set
            {
                proxyItem = value;
                
            }
        }

        private string fullName;
        public string FullName
        {
            get { return fullName; }
            set
            {
                fullName = value;
                RaisePropertyChanged(NotifyChanges.ProcessTreeNode_FullName);
            }
        }

        public ProcessTreeNode ParentNode { get; private set; }

        private List<ProcessTreeNode> childNodes;
        public List<ProcessTreeNode> ChildNodes
        {
            get { return childNodes; }
            set
            {
                childNodes = value;
                RaisePropertyChanged(NotifyChanges.ProcessTreeNode_ChildNodes);
            }
        }

        public List<ProcessTreeNode> AllChildNodes { get; private set; }

        public ProcessTreeNode(clsOntologyItem proxyItem, clsLocalConfig localConfig, DataCollector_ProcessTree dataCollectorProcessTree, ProcessTreeNode parentNode = null)
        {

            this.localConfig = localConfig;
            ProxyItem = proxyItem;
            ParentNode = parentNode;
            FullName = (ParentNode != null ? ParentNode.fullName : "") + ProxyItem.GUID;
            this.dataCollectorProcessTree = dataCollectorProcessTree;
            System.Diagnostics.Debug.Print(proxyItem.GUID + " " + proxyItem.Name);   
            AllChildNodes = new List<ProcessTreeNode>();
            if (proxyItem == null) throw new OntologicalException(localConfig.LocalizeGui.GetMessageItem(localConfig.OItem_object_the_processtree_node_must_not_be_empty.GUID,"en"));
            GetSubProcesses();
        }

        private void GetSubProcesses()
        {
            
            var subProcessesRel = dataCollectorProcessTree.GetData_SubProcesses(ProxyItem);
            if (subProcessesRel != null)
            {
                var newSubProcesses = (from subProc in subProcessesRel.Where(subprocRel => subprocRel.ID_Other != ProxyItem.GUID)
                                       join childNodeExist in AllChildNodes on subProc.ID_Other equals childNodeExist.IdProcessTreeNode into childNodesExist
                                       from childNodeExist in childNodesExist.DefaultIfEmpty()
                                       where childNodeExist == null
                                       select subProc).ToList();
                ChildNodes = newSubProcesses.Select(subProc => new ProcessTreeNode(new clsOntologyItem
                {
                    GUID = subProc.ID_Other,
                    Name = subProc.Name_Other,
                    GUID_Parent = subProc.ID_Parent_Other,
                    Type = subProc.Ontology
                }, localConfig, dataCollectorProcessTree, this)).ToList();
                AllChildNodes.AddRange(ChildNodes);
            }
            else
            {
                throw new OntologicalException(localConfig.LocalizeGui.GetMessageItem(localConfig.OItem_object_error_while_loading_subprocesses.GUID, "en"));
            }

        }
        
        

        
    }
}
