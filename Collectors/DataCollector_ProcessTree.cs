﻿using ImageMapper_Module.BaseClasses;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using ProcessTree_Module.Communications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProcessTree_Module.Collectors
{
    public class DataCollector_ProcessTree : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private object locker1 = new object();
        private object locker2 = new object();
        private object locker3 = new object();
        private object locker4 = new object();
        private object locker5 = new object();

        private clsOntologyItem oItemProc1;
        private clsOntologyItem oItemProc2;
        private clsOntologyItem oItemProc3;
        private clsOntologyItem oItemProc4;
        private clsOntologyItem oItemProc5;

        private OntologyModDBConnector oReader_SubProcesses1;
        private OntologyModDBConnector oReader_SubProcesses2;
        private OntologyModDBConnector oReader_SubProcesses3;
        private OntologyModDBConnector oReader_SubProcesses4;
        private OntologyModDBConnector oReader_SubProcesses5;

        private clsOntologyItem result_LoadPublicProcesses;
        public clsOntologyItem Result_LoadPublicProcesses
        {
            get { return result_LoadPublicProcesses; }
            private set
            {
                result_LoadPublicProcesses = value;
                RaisePropertyChanged(NotifyChanges.DataCollector_ProcessTree_Result_LoadPublicProcesses);
            }
        }

        private List<clsOntologyItem> publicProcesses;
        public List<clsOntologyItem> PublicProcesses
        {
            get
            {

                lock (locker1)
                {
                    return publicProcesses;
                }
                
            }
            set
            {
                lock(locker1)
                {
                    publicProcesses = value;
                }
                
                RaisePropertyChanged(NotifyChanges.DataCollector_ProcessTree_PublicProcesses);
            }
        }

        private Thread publicProcessThread;

        private Thread subProcessThread1;
        private Thread subProcessThread2;
        private Thread subProcessThread3;
        private Thread subProcessThread4;
        private Thread subProcessThread5;

        public void GetData_SubProcesses(clsOntologyItem parentProcess)
        {
            int threadNumber = 0;
            while (threadNumber == 0)
            {
                threadNumber = GetFreeThreadNumber();
            }

            switch (threadNumber)
            {
                case 1:

                    break;
                case 2:

                    break;
                case 3:

                    break;
                case 4:

                    break;
                case 5:

                    break;
            }

        }

        public int GetFreeThreadNumber()
        {
            if (subProcessThread1 == null || subProcessThread1.ThreadState != ThreadState.Running)
            {
                return 1;
            }
            else if (subProcessThread2 == null || subProcessThread1.ThreadState != ThreadState.Running)
            {
                return 2;
            }
            else if (subProcessThread3 == null || subProcessThread3.ThreadState != ThreadState.Running)
            {
                return 3;
            }
            else if (subProcessThread4 == null || subProcessThread4.ThreadState != ThreadState.Running)
            {
                return 4;
            }
            else if (subProcessThread4 == null || subProcessThread5.ThreadState != ThreadState.Running)
            {
                return 5;
            }

            return 0;
        }


        public void GetData_SubProcess1(clsOntologyItem childProcesses)
        {
            lock(locker1)
            {

            }
        }


        public clsOntologyItem GetData_PublicProcesses()
        {
            if (publicProcessThread != null)
            {
                try
                {
                    publicProcessThread.Abort();
                }
                catch (Exception ex) { }
            }

            Result_LoadPublicProcesses = localConfig.Globals.LState_Nothing.Clone();
            PublicProcesses = new List<clsOntologyItem>();

            publicProcessThread = new Thread(LoadThread_PublicProcesses);
            publicProcessThread.Start();

            return localConfig.Globals.LState_Success.Clone();
        }
    
        private void LoadThread_PublicProcesses()
        {
            var oReader = new OntologyModDBConnector(localConfig.Globals);

            var searchPublicProcesses = new List<clsObjectAtt> {
                new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attributetype_public.GUID,
                    ID_Class = localConfig.OItem_class_process.GUID
                }
            };

            var result = oReader.GetDataObjectAtt(searchPublicProcesses);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                PublicProcesses = oReader.ObjAtts.Select(proc => new clsOntologyItem
                {
                    GUID = proc.ID_Object,
                    Name = proc.Name_Object,
                    GUID_Parent = proc.ID_Class,
                    Type = localConfig.Globals.Type_Object
                }).ToList();
            }

            Result_LoadPublicProcesses = result;
        }

        public DataCollector_ProcessTree(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            oReader_SubProcesses = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
