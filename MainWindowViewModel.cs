﻿using OntologyAppDBConnector;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace ProcessTree_Module
{
    public class MainWindowViewModel
    {
        private clsLocalConfig localConfig;

        public void CreatePublisTree()
        {
            localConfig.ProcessTreeNodePublisher.ExchangeOItem(localConfig.Globals.Root);
        }

        public MainWindowViewModel()
        {
            localConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (localConfig == null)
            {
                localConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(localConfig);
            }
        }
    }
}
