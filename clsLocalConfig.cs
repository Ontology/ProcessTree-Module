﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using OntologyClasses.Interfaces;
using ImageMapper_Module;
using Localization_Module;
using OntoMsg_Module;

namespace ProcessTree_Module
{
    public class clsLocalConfig : ILocalConfig, IGuiLocalization
{
    private const string cstrID_Ontology = "e9b7d92447b74c1caa014c8a9dce3562";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }
        public clsOntologyItem OItem_Development { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

        public ObjectPublisher ProcessTreeNodePublisher { get; private set; }

        // AttributeType
        public clsOntologyItem OItem_attributetype_public { get; set; }

        // Classes
        public clsOntologyItem OItem_class_incident { get; set; }
    public clsOntologyItem OItem_class_process { get; set; }
    public clsOntologyItem OItem_class_process_log { get; set; }
    public clsOntologyItem OItem_class_process_ticket { get; set; }

        // Objects
        public clsOntologyItem OItem_object_processtree { get; set; }
        public clsOntologyItem OItem_object_the_processtree_node_must_not_be_empty { get; set; }
        public clsOntologyItem OItem_object_error_while_loading_subprocesses { get; set; }

        // RelationTypes
        public clsOntologyItem OItem_relationtype_superordinate { get; set; }

        private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
            GetImageItems();
                GetLocalizationItems();
                SetPublishers();
        }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }
            if (MessageBox.Show(strTitle + ": Die notwendigen Basisdaten konnten nicht geladen werden! Soll versucht werden, sie in der Datenbank " +
                      Globals.Index + "@" + Globals.Server + " zu erzeugen?", "Datenstrukturen", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                var objOItem_Result = objImport.ImportTemplates(objAssembly);
                if (objOItem_Result.GUID != Globals.LState_Error.GUID)
                {
                    get_Data_DevelopmentConfig();
                    get_Config_AttributeTypes();
                    get_Config_RelationTypes();
                    get_Config_Classes();
                    get_Config_Objects();
                }
                else
                {
                    throw new Exception("Config not importable");
                }
            }
            else
            {
                Environment.Exit(0);
            }
        }
    }

    private void get_Config_AttributeTypes()
    {
            var objOList_attributetype_public = (from objOItem in objDBLevel_Config1.ObjectRels
                                                 where objOItem.ID_Object == cstrID_Ontology
                                                 join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                 where objRef.Name_Object.ToLower() == "attributetype_public".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                 select objRef).ToList();

            if (objOList_attributetype_public.Any())
            {
                OItem_attributetype_public = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_public.First().ID_Other,
                    Name = objOList_attributetype_public.First().Name_Other,
                    GUID_Parent = objOList_attributetype_public.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_short = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attributetype_short".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

            if (objOList_attributetype_short.Any())
            {
                OItem_attributetype_short = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_short.First().ID_Other,
                    Name = objOList_attributetype_short.First().Name_Other,
                    GUID_Parent = objOList_attributetype_short.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "attributetype_message".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                  select objRef).ToList();

            if (objOList_attributetype_message.Any())
            {
                OItem_attributetype_message = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_message.First().ID_Other,
                    Name = objOList_attributetype_message.First().Name_Other,
                    GUID_Parent = objOList_attributetype_message.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_caption = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "attributetype_caption".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                  select objRef).ToList();

            if (objOList_attributetype_caption.Any())
            {
                OItem_attribute_caption = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_caption.First().ID_Other,
                    Name = objOList_attributetype_caption.First().Name_Other,
                    GUID_Parent = objOList_attributetype_caption.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

        public List<ImageItem> ImageList { get; private set; }

    private void get_Config_RelationTypes()
    {
            var objOList_relationtype_superordinate = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "relationtype_superordinate".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                       select objRef).ToList();

            if (objOList_relationtype_superordinate.Any())
            {
                OItem_relationtype_superordinate = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_superordinate.First().ID_Other,
                    Name = objOList_relationtype_superordinate.First().Name_Other,
                    GUID_Parent = objOList_relationtype_superordinate.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_user_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_user_message".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

            if (objOList_relationtype_user_message.Any())
            {
                OItem_relationtype_user_message = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_user_message.First().ID_Other,
                    Name = objOList_relationtype_user_message.First().Name_Other,
                    GUID_Parent = objOList_relationtype_user_message.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is_written_in = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "relationtype_is_written_in".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                       select objRef).ToList();

            if (objOList_relationtype_is_written_in.Any())
            {
                OItem_relationtype_iswrittenin = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_written_in.First().ID_Other,
                    Name = objOList_relationtype_is_written_in.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_written_in.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is_defined_by = (from objOItem in objDBLevel_Config1.ObjectRels
                                                       where objOItem.ID_Object == cstrID_Ontology
                                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                       where objRef.Name_Object.ToLower() == "relationtype_is_defined_by".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                       select objRef).ToList();

            if (objOList_relationtype_is_defined_by.Any())
            {
                OItem_relationtype_is_defined_by = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_defined_by.First().ID_Other,
                    Name = objOList_relationtype_is_defined_by.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_defined_by.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_inputmessage = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_inputmessage".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

            if (objOList_relationtype_inputmessage.Any())
            {
                OItem_relationtype_inputmessage = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_inputmessage.First().ID_Other,
                    Name = objOList_relationtype_inputmessage.First().Name_Other,
                    GUID_Parent = objOList_relationtype_inputmessage.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_errormessage = (from objOItem in objDBLevel_Config1.ObjectRels
                                                      where objOItem.ID_Object == cstrID_Ontology
                                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                      where objRef.Name_Object.ToLower() == "relationtype_errormessage".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                      select objRef).ToList();

            if (objOList_relationtype_errormessage.Any())
            {
                OItem_relationtype_errormessage = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_errormessage.First().ID_Other,
                    Name = objOList_relationtype_errormessage.First().Name_Other,
                    GUID_Parent = objOList_relationtype_errormessage.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_contains.Any())
            {
                OItem_relationtype_contains = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_contains.First().ID_Other,
                    Name = objOList_relationtype_contains.First().Name_Other,
                    GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belongs_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belongs_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_belongs_to.Any())
            {
                OItem_relationtype_belongsto = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belongs_to.First().ID_Other,
                    Name = objOList_relationtype_belongs_to.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belongs_to.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

        private void GetImageItems()
        {
            var imageMapperEngine = new ImageMapperEngine(Globals);
            var result = imageMapperEngine.GetImagesOfImageList(OItem_object_processtree);
            if (result.GUID == Globals.LState_Success.GUID)
            {
                while (!imageMapperEngine.ListLoaded) ;

                ImageList = imageMapperEngine.ImageItemList;

                if (ImageList == null || !ImageList.Any())
                {
                    throw new Exception("config err");
                }
            }
            else
            {
                throw new Exception("config err");
            }
        }

        public clsLocalizeGui LocalizeGui
        {
            get; private set;
        }

        private void GetLocalizationItems()
        {
            var searchSoftwareDevelopment = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = cstrID_Ontology,
                    ID_RelationType = Globals.RelationType_belongingResource.GUID
                }

            };

            var result = objDBLevel_Config1.GetDataObjectRel(searchSoftwareDevelopment);

            if (result.GUID == Globals.LState_Success.GUID)
            {
                OItem_Development = objDBLevel_Config1.ObjectRels.Select(ontToDev => new clsOntologyItem
                {
                    GUID = ontToDev.ID_Other,
                    Name = ontToDev.Name_Other,
                    GUID_Parent = ontToDev.ID_Parent_Other,
                    Type = ontToDev.Ontology
                }).FirstOrDefault();

                if (OItem_Development == null)
                {
                    throw new Exception("config err");
                }
            }
            else
            {
                throw new Exception("config err");
            }

            LocalizeGui = new clsLocalizeGui(this);
        }

        private void SetPublishers()
        {
            ProcessTreeNodePublisher = MessageManager.RegisterPublisher(Guid.NewGuid().ToString().Replace("-", ""));
            if (ProcessTreeNodePublisher == null)
            {
                throw new Exception("config err");
            }
        }
    private void get_Config_Objects()
    {
            var objOList_object_error_while_loading_subprocesses = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                    where objOItem.ID_Object == cstrID_Ontology
                                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                    where objRef.Name_Object.ToLower() == "object_error_while_loading_subprocesses".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                    select objRef).ToList();

            if (objOList_object_error_while_loading_subprocesses.Any())
            {
                OItem_object_error_while_loading_subprocesses = new clsOntologyItem()
                {
                    GUID = objOList_object_error_while_loading_subprocesses.First().ID_Other,
                    Name = objOList_object_error_while_loading_subprocesses.First().Name_Other,
                    GUID_Parent = objOList_object_error_while_loading_subprocesses.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_the_processtree_node_must_not_be_empty = (from objOItem in objDBLevel_Config1.ObjectRels
                                                                          where objOItem.ID_Object == cstrID_Ontology
                                                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                                          where objRef.Name_Object.ToLower() == "object_the_processtree_node_must_not_be_empty".ToLower() && objRef.Ontology == Globals.Type_Object
                                                                          select objRef).ToList();

            if (objOList_object_the_processtree_node_must_not_be_empty.Any())
            {
                OItem_object_the_processtree_node_must_not_be_empty = new clsOntologyItem()
                {
                    GUID = objOList_object_the_processtree_node_must_not_be_empty.First().ID_Other,
                    Name = objOList_object_the_processtree_node_must_not_be_empty.First().Name_Other,
                    GUID_Parent = objOList_object_the_processtree_node_must_not_be_empty.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_processtree = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "object_processtree".ToLower() && objRef.Ontology == Globals.Type_Object
                                               select objRef).ToList();

            if (objOList_object_processtree.Any())
            {
                OItem_object_processtree = new clsOntologyItem()
                {
                    GUID = objOList_object_processtree.First().ID_Other,
                    Name = objOList_object_processtree.First().Name_Other,
                    GUID_Parent = objOList_object_processtree.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

        }

    private void get_Config_Classes()
    {
            var objOList_class_tooltip_messages = (from objOItem in objDBLevel_Config1.ObjectRels
                                                   where objOItem.ID_Object == cstrID_Ontology
                                                   join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                   where objRef.Name_Object.ToLower() == "class_tooltip_messages".ToLower() && objRef.Ontology == Globals.Type_Class
                                                   select objRef).ToList();

            if (objOList_class_tooltip_messages.Any())
            {
                OItem_type_tooltip_messages = new clsOntologyItem()
                {
                    GUID = objOList_class_tooltip_messages.First().ID_Other,
                    Name = objOList_class_tooltip_messages.First().Name_Other,
                    GUID_Parent = objOList_class_tooltip_messages.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_messages = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "class_messages".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_class_messages.Any())
            {
                OItem_class_messages = new clsOntologyItem()
                {
                    GUID = objOList_class_messages.First().ID_Other,
                    Name = objOList_class_messages.First().Name_Other,
                    GUID_Parent = objOList_class_messages.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_localized_message = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "class_localized_message".ToLower() && objRef.Ontology == Globals.Type_Class
                                                    select objRef).ToList();

            if (objOList_class_localized_message.Any())
            {
                OItem_class_localized_message = new clsOntologyItem()
                {
                    GUID = objOList_class_localized_message.First().ID_Other,
                    Name = objOList_class_localized_message.First().Name_Other,
                    GUID_Parent = objOList_class_localized_message.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_language = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "class_language".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_class_language.Any())
            {
                OItem_type_language = new clsOntologyItem()
                {
                    GUID = objOList_class_language.First().ID_Other,
                    Name = objOList_class_language.First().Name_Other,
                    GUID_Parent = objOList_class_language.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_gui_entires = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "class_gui_entires".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

            if (objOList_class_gui_entires.Any())
            {
                OItem_type_gui_entires = new clsOntologyItem()
                {
                    GUID = objOList_class_gui_entires.First().ID_Other,
                    Name = objOList_class_gui_entires.First().Name_Other,
                    GUID_Parent = objOList_class_gui_entires.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_gui_caption = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "class_gui_caption".ToLower() && objRef.Ontology == Globals.Type_Class
                                              select objRef).ToList();

            if (objOList_class_gui_caption.Any())
            {
                OItem_type_gui_caption = new clsOntologyItem()
                {
                    GUID = objOList_class_gui_caption.First().ID_Other,
                    Name = objOList_class_gui_caption.First().Name_Other,
                    GUID_Parent = objOList_class_gui_caption.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }


            var objOList_class_incident = (from objOItem in objDBLevel_Config1.ObjectRels
                                       where objOItem.ID_Object == cstrID_Ontology
                                       join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                       where objRef.Name_Object.ToLower() == "class_incident".ToLower() && objRef.Ontology == Globals.Type_Class
                                       select objRef).ToList();

        if (objOList_class_incident.Any())
        {
            OItem_class_incident = new clsOntologyItem()
            {
                GUID = objOList_class_incident.First().ID_Other,
                Name = objOList_class_incident.First().Name_Other,
                GUID_Parent = objOList_class_incident.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_process = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "class_process".ToLower() && objRef.Ontology == Globals.Type_Class
                                      select objRef).ToList();

        if (objOList_class_process.Any())
        {
            OItem_class_process = new clsOntologyItem()
            {
                GUID = objOList_class_process.First().ID_Other,
                Name = objOList_class_process.First().Name_Other,
                GUID_Parent = objOList_class_process.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_process_log = (from objOItem in objDBLevel_Config1.ObjectRels
                                          where objOItem.ID_Object == cstrID_Ontology
                                          join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                          where objRef.Name_Object.ToLower() == "class_process_log".ToLower() && objRef.Ontology == Globals.Type_Class
                                          select objRef).ToList();

        if (objOList_class_process_log.Any())
        {
            OItem_class_process_log = new clsOntologyItem()
            {
                GUID = objOList_class_process_log.First().ID_Other,
                Name = objOList_class_process_log.First().Name_Other,
                GUID_Parent = objOList_class_process_log.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_class_process_ticket = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "class_process_ticket".ToLower() && objRef.Ontology == Globals.Type_Class
                                             select objRef).ToList();

        if (objOList_class_process_ticket.Any())
        {
            OItem_class_process_ticket = new clsOntologyItem()
            {
                GUID = objOList_class_process_ticket.First().ID_Other,
                Name = objOList_class_process_ticket.First().Name_Other,
                GUID_Parent = objOList_class_process_ticket.First().ID_Parent_Other,
                Type = Globals.Type_Class
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }

        public clsOntologyItem OItem_attribute_caption
        {
            get; set;
            
        }

        public clsOntologyItem OItem_attributetype_message
        {
            get;  set;
        }

        public clsOntologyItem OItem_attributetype_short
        {
            get;  set;
        }

        public clsOntologyItem OItem_type_gui_caption
        {
            get;  set;
        }

        public clsOntologyItem OItem_type_gui_entires
        {
            get;  set;
        }

        public clsOntologyItem OItem_type_language
        {
            get;  set;
        }

        public clsOntologyItem OItem_class_localized_message
        {
            get;  set;
        }

        public clsOntologyItem OItem_class_messages
        {
            get;  set;
        }

        public clsOntologyItem OItem_type_tooltip_messages
        {
            get;  set;
        }

        public clsOntologyItem OItem_relationtype_belongsto
        {
            get;  set;
        }

        public clsOntologyItem OItem_relationtype_contains
        {
            get;  set;
        }

        public clsOntologyItem OItem_relationtype_errormessage
        {
            get;  set;
        }

        public clsOntologyItem OItem_relationtype_user_message
        {
            get;  set;
        }

        public clsOntologyItem OItem_relationtype_inputmessage
        {
            get;  set;
        }

        public clsOntologyItem OItem_relationtype_is_defined_by
        {
            get;  set;
        }

        public clsOntologyItem OItem_relationtype_iswrittenin
        {
            get;  set;
        }

        public string Id_Development
        {
            get
            {
                return OItem_Development.GUID;
            }
        }
    }

}